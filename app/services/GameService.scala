package services

import game.{Board, GameSave}
import javax.inject._

import players.{ComputerPlayer, Direction, HumanPlayer, Player}

trait GameService {
  def newHumanGame()
  def newComputerGame(level : Int)
  def move(direction: Direction)
  def getBoard: Board
  def aiMove()
  def load(name: String)
  def save(name: String)
  def saves(): List[GameSave]
}

@Singleton
class GameServiceImpl extends GameService {
  var board: Board = new Board(Array.ofDim(4, 4), 0, false, false)
  var player: Player = HumanPlayer


  override def newHumanGame() = {
    board = new Board(Array.ofDim(4, 4), 0, false, false)
    board.initialize()
    player = HumanPlayer
  }

  override def newComputerGame(level : Int) = {
    board = new Board(Array.ofDim(4, 4), 0, false, false)
    board.initialize()
    player = new ComputerPlayer(level)
  }

  override def move(direction: Direction) = {
    if (board.canPerformMove && !board.contains2048) {
      board = player.makeMove(board, direction)
    }
  }

  override def getBoard = board

  override def aiMove() = {
    if (board.canPerformMove && !board.contains2048) {
      board = player.makeMove(board)
      board.newElement()
    }
  }

  override def load(name : String) = {
    board = new Board(Array.ofDim(4, 4), 0, false, false)
    board = player.loadBoard(board, name)
  }

  override def save(name : String) = {
    board = player.saveBoard(board, name)
  }

  override def saves() = {
    player.getSaves()
  }
}

