package game

import play.api.libs.json.{JsValue, Json, Writes}
import reactivemongo.bson.{BSONDocument, BSONDocumentReader, BSONObjectID}
/**
  * @author Tobiasz Kowalski, ATOS Poland on 10.07.2016
  */
case class GameSave(id: BSONObjectID, name: String, game: String, score: Int)

object GameSave {
  implicit object PersonReader extends BSONDocumentReader[GameSave] {
    def read(doc: BSONDocument): GameSave = {
      val id = doc.getAs[BSONObjectID]("_id").get
      val name = doc.getAs[String]("name").get
      val game = doc.getAs[String]("game").get
      val score = doc.getAs[Int]("score").get

      GameSave(id, name, game, score)
    }
  }

  implicit val writer = new Writes[GameSave] {
    def writes(game: GameSave): JsValue = {
      Json.obj("name" -> game.name)
    }
  }
}