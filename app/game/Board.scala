package game

import scala.util.Random
import play.api.libs.json.Json

case class Board(board: Array[Array[Int]], score: Int, is2048: Boolean, canMove: Boolean) {
  private val r = Random

  def equalBoards(otherBoard: Board): Boolean = {
    var equal = true
    for (row <- 0 to 3) {
      for (column <- 0 to 3) {
        if (otherBoard.getBoard(row)(column) != board(row)(column)) {
          equal = false
        }
      }
    }
    equal
  }

  def initialize(): Unit = {
    newElement()
    newElement()
  }

  def show(): Unit = {
    println("Score: " + score)
    board foreach {
      case row => row foreach {
        column => printf("%4d ", column)
      }
        println
    }
    println("_____________________")
  }

  def copy(): Array[Array[Int]] = {
    val copiedBoard = Array.ofDim[Int](4, 4)
    for (row <- 0 to 3) {
      for (column <- 0 to 3) {
        copiedBoard(row)(column) = board(row)(column)
      }
    }
    copiedBoard
  }

  def newElement(): Unit = {
    var initialized = false
    do {
      val row: Int = (r.nextDouble() * 4).toInt
      val column: Int = (r.nextDouble() * 4).toInt
      if (board(row)(column) == 0) {
        board(row)(column) = if (r.nextDouble() < 0.9) 2 else 4
        initialized = true
      }
    } while (!initialized)
  }

  def canPerformMove: Boolean = {
    if (countEmptyElements > 0 || hasPair) {
      return true
    }
    false
  }

  def contains2048: Boolean = {
    for (column <- 0 to 3) {
      for (row <- 0 to 3) {
        if (board(row)(column) == 2048) {
          return true
        }
      }
    }
    false
  }

  def countEmptyElements: Int = {
    var emptyElements = 0
    for (column <- 0 to 3) {
      for (row <- 0 to 3) {
        if (board(row)(column) == 0) {
          emptyElements += 1
        }
      }
    }
    emptyElements
  }

  private def hasPair: Boolean = {
    for (row <- 0 to 3) {
      for (column <- 0 to 3) {
        if (column + 1 <= 3 && board(row)(column + 1) == board(row)(column)) {
          return true
        }
        if (row + 1 <= 3 && board(row + 1)(column) == board(row)(column)) {
          return true
        }
      }
    }
    false
  }

  def getBoard: Array[Array[Int]] = {
    board
  }

  def getScore: Int = {
    score
  }

  def convertToBoard(values: String): Array[Array[Int]] = {
    val array = values.split(",").map(_.toInt)

    val board = Array.ofDim[Int](4, 4)

    for (i <- 0 to 3) {
      for (j <- 0 to 3) {
        board(i)(j) = array((i * 4) + j)
      }
    }

    board
  }
}

object Board {
  implicit val boardFormat = Json.format[Board]
}
