package controllers


import javax.inject._
import game.Board
import play.api.libs.json.Json
import play.api.mvc._
import players.Direction
import services.GameService


@Singleton
class GameController @Inject() (gameService: GameService) extends Controller  {

  def index = Action {
    Ok(views.html.index())
  }

  def newHumanGame = Action {
    gameService.newHumanGame()
    Ok(Json.toJson(gameService.getBoard))
  }

  def newComputerGame(level : Int) = Action {
    gameService.newComputerGame(level)
    Ok(Json.toJson(gameService.getBoard))
  }

  def aiMove = Action {
    gameService.aiMove()
    val newBoard = new Board(gameService.getBoard.getBoard, gameService.getBoard.getScore, gameService.getBoard.contains2048, gameService.getBoard.canPerformMove)
    Ok(Json.toJson(newBoard))
  }

  def moveUp = Action {
    gameService.move(Direction.UP)
    Ok(Json.toJson(gameService.getBoard))
  }

  def moveDown = Action {
    gameService.move(Direction.DOWN)
    Ok(Json.toJson(gameService.getBoard))
  }

  def moveLeft = Action {
    gameService.move(Direction.LEFT)
    Ok(Json.toJson(gameService.getBoard))
  }

  def moveRight = Action {
    gameService.move(Direction.RIGHT)
    Ok(Json.toJson(gameService.getBoard))
  }

  def load(name : String) = Action {
    gameService.load(name)
    Ok(Json.toJson(gameService.getBoard))
  }

  def save(name : String) = Action {
    gameService.save(name : String)
    Ok(Json.toJson(gameService.getBoard))
  }

  def saves = Action {
    Ok(Json.toJson(gameService.saves()))
  }
}
