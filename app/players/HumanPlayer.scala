package players

import game.{Board, GameSave}
import reactivemongo.api.MongoDriver
import reactivemongo.api.collections.bson.BSONCollection
import reactivemongo.bson.BSONDocument

import scala.concurrent.Await
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.io.StdIn

object HumanPlayer extends Player {
  override def makeMove(board: Board, direction: Direction): Board = {
    val beforeBoard = board.copy()
    var newBoard = board
    direction match {
      case Direction.UP => newBoard = moveUp(board)
      case Direction.DOWN => newBoard = moveDown(board)
      case Direction.LEFT => newBoard = moveLeft(board)
      case Direction.RIGHT => newBoard = moveRight(board)
      case _ => newBoard = moveUp(board)
    }

    if (!board.equalBoards(newBoard)){
      newBoard.newElement()
    }
    newBoard = new Board(newBoard.getBoard, newBoard.getScore, newBoard.contains2048, newBoard.canPerformMove)
    newBoard
  }

  override def makeMove(board: Board): Board = {
    return null
  }

  private def printBoard(board: String) {
    val values = board.split(",")
    for (i <- 0 until values.length) {
      if (i % 4 == 0) {
        println()
      }

      printf("%4s ", values(i))
    }

    println()
    println("_____________________")
  }
}
