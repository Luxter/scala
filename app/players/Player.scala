package players

import scala.util.{Failure, Success}
import game.{Board, GameSave}
import reactivemongo.api.MongoDriver
import reactivemongo.api.collections.bson.BSONCollection
import reactivemongo.bson.BSONDocument

import scala.concurrent.Await
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.util.control.Breaks._

trait Player {

  def makeMove(board: Board, direction: Direction): Board

  def makeMove(board: Board): Board

  def moveUp(board: Board): Board = {
    var newBoard = performMoveUp(board)
    newBoard = performMergeUp(newBoard)
    newBoard = performMoveUp(newBoard)
    newBoard
  }

  def moveRight(board: Board): Board = {
    var newBoard = performMoveRight(board)
    newBoard = performMergeRight(newBoard)
    newBoard = performMoveRight(newBoard)
    newBoard
  }

  def moveDown(board: Board): Board = {
    var newBoard = performMoveDown(board)
    newBoard = performMergeDown(newBoard)
    newBoard = performMoveDown(newBoard)
    newBoard
  }

  def moveLeft(board: Board): Board = {
    var newBoard = performMoveLeft(board)
    newBoard = performMergeLeft(newBoard)
    newBoard = performMoveLeft(newBoard)
    newBoard
  }

  private def performMoveUp(board: Board): Board = {
    val copiedBoard = board.copy()
    for (column <- 0 to 3) {
      for (row <- 0 to 2) {
        if (copiedBoard(row)(column) == 0) {
          breakable {
            for (row2 <- row + 1 to 3) {
              if (copiedBoard(row2)(column) != 0) {
                copiedBoard(row)(column) = copiedBoard(row2)(column)
                copiedBoard(row2)(column) = 0
                break
              }
            }
          }
        }
      }
    }
    new Board(copiedBoard, board.getScore, board.contains2048, board.canPerformMove)
  }

  def getSaves(): List[GameSave] = {
    val driver = new MongoDriver
    val db = Await.result(driver.connection(List("localhost")).database("2048GameDB"), 1.second)
    val collection = db[BSONCollection]("GameSave")

    val data = collection
      .find(BSONDocument())
      .cursor[GameSave]()
      .collect[List]()

    Await.result(data, 1.second)
  }


  def saveBoard(board: Board, name: String): Board = {
    val driver = new MongoDriver
    val db = Await.result(driver.connection(List("localhost")).database("2048GameDB"), 1.second)
    val collection = db[BSONCollection]("GameSave")

    val future = collection.insert(BSONDocument(
      "name" -> name,
      "game" -> convertBoard(board),
      "score" -> board.score))

    new Board(board.copy(), board.getScore, board.contains2048, board.canPerformMove)
  }

  def loadBoard(board: Board, name : String): Board = {
    val driver = new MongoDriver
    val db = Await.result(driver.connection(List("localhost")).database("2048GameDB"), 1.second)
    val collection = db[BSONCollection]("GameSave")

    val data = collection
      .find(BSONDocument())
      .cursor[GameSave]()
      .collect[List]()

    val gameSaves: List[GameSave] = Await.result(data, 1.second)

    if(!gameSaves.exists(game => game.name == name)) {
      return board
    }

    val game = gameSaves.filter(save => save.name == name).head.game
    val score = gameSaves.filter(save => save.name == name).head.score

    new Board(board.convertToBoard(game), score, board.contains2048, board.canPerformMove)
  }

  private def convertBoard(board : Board): String = {
    var values: String = ""

    board.board foreach {
      case row => row foreach {
        column => values += column + ","
      }
    }

    values
  }

  private def performMergeUp(board: Board): Board = {
    val copiedBoard = board.copy()
    var actualScore = board.getScore
    for (column <- 0 to 3) {
      for (row <- 0 to 2) {
        if (copiedBoard(row)(column) != 0 && copiedBoard(row)(column) == copiedBoard(row + 1)(column)) {
          copiedBoard(row)(column) <<= 1
          actualScore += copiedBoard(row)(column)
          copiedBoard(row + 1)(column) = 0
        }
      }
    }
    new Board(copiedBoard, actualScore, false, false)
  }

  private def performMoveRight(board: Board): Board = {
    val copiedBoard = board.copy()
    for (row <- 0 to 3) {
      for (column <- 3 to 1 by -1) {
        if (copiedBoard(row)(column) == 0) {
          breakable {
            for (column2 <- column - 1 to 0 by -1) {
              if (copiedBoard(row)(column2) != 0) {
                copiedBoard(row)(column) = copiedBoard(row)(column2)
                copiedBoard(row)(column2) = 0
                break
              }
            }
          }
        }
      }
    }
    new Board(copiedBoard, board.getScore, false, false)
  }

  private def performMergeRight(board: Board): Board = {
    val copiedBoard = board.copy()
    var actualScore = board.getScore
    for (row <- 0 to 3) {
      for (column <- 3 to 1 by -1) {
        if (copiedBoard(row)(column) != 0 && copiedBoard(row)(column) == copiedBoard(row)(column - 1)) {
          copiedBoard(row)(column) <<= 1
          actualScore += copiedBoard(row)(column)
          copiedBoard(row)(column - 1) = 0
        }
      }
    }
    new Board(copiedBoard, actualScore, false, false)
  }

  private def performMoveDown(board: Board): Board = {
    val copiedBoard = board.copy()
    for (column <- 0 to 3) {
      for (row <- 3 to 1 by -1) {
        if (copiedBoard(row)(column) == 0) {
          breakable {
            for (row2 <- row - 1 to 0 by -1) {
              if (copiedBoard(row2)(column) != 0) {
                copiedBoard(row)(column) = copiedBoard(row2)(column)
                copiedBoard(row2)(column) = 0
                break
              }
            }
          }
        }
      }
    }
    new Board(copiedBoard, board.getScore, false, false)
  }

  private def performMergeDown(board: Board): Board = {
    val copiedBoard = board.copy()
    var actualScore = board.getScore
    for (column <- 0 to 3) {
      for (row <- 3 to 1 by -1) {
        if (copiedBoard(row)(column) != 0 && copiedBoard(row)(column) == copiedBoard(row - 1)(column)) {
          copiedBoard(row)(column) <<= 1
          actualScore += copiedBoard(row)(column)
          copiedBoard(row - 1)(column) = 0
        }
      }
    }
    new Board(copiedBoard, actualScore, false, false)
  }

  private def performMoveLeft(board: Board): Board = {
    val copiedBoard = board.copy()
    for (row <- 0 to 3) {
      for (column <- 0 to 2) {
        if (copiedBoard(row)(column) == 0) {
          breakable {
            for (column2 <- column + 1 to 3) {
              if (copiedBoard(row)(column2) != 0) {
                copiedBoard(row)(column) = copiedBoard(row)(column2)
                copiedBoard(row)(column2) = 0
                break
              }
            }
          }
        }
      }
    }
    new Board(copiedBoard, board.getScore, false, false)
  }

  private def performMergeLeft(board: Board): Board = {
    val copiedBoard = board.copy()
    var actualScore = board.getScore
    for (row <- 0 to 3) {
      for (column <- 0 to 2) {
        if (copiedBoard(row)(column) != 0 && copiedBoard(row)(column) == copiedBoard(row)(column + 1)) {
          copiedBoard(row)(column) <<= 1
          actualScore += copiedBoard(row)(column)
          copiedBoard(row)(column + 1) = 0
        }
      }
    }
    new Board(copiedBoard, actualScore, false, false)
  }
}
