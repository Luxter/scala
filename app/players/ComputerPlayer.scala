package players

import game.Board
import players.HumanPlayer._

import scala.collection.mutable
import util.control.Breaks._

import scala.util.Random

class ComputerPlayer(var movesAhead: Int) extends Player {
  var originalDepth = 2 * movesAhead

  private val r = Random
  private val randomMovesScore = 1000

  case class Node(board: Board, depth: Int, var direction: Int, probability: Double) {

    val childs: mutable.MutableList[Node] = mutable.MutableList[Node]()

    def value(): (Double, Int) = {
      if (depth == 0) {
        heuristicValue()
      } else {
        if (depth % 2 == 0) {
          maxValue()
        } else {
          expectedValue()
        }
      }
    }

    def addChild(node: Node): Unit = {
      childs += node
    }

    private def maxValue(): (Double, Int) = {
      var maxValue = -1.0
      for (child <- childs) {
        val childValue = child.value()._1
        if (childValue > maxValue) {
          maxValue = childValue
          direction = child.direction
        }
      }
      (maxValue, direction)
    }

    private def expectedValue(): (Double, Int) = {
      var expectedValue = 0.0
      for (child <- childs) {
        expectedValue += child.probability * child.value()._1
      }
      expectedValue /= childs.size
      (expectedValue, direction)
    }

    private def heuristicValue(): (Double, Int) = {
      if(!board.canPerformMove){
        (0, direction)
      }
      (board.getScore + Math.log(board.getScore) * emptyCountValue() - averageNeighbourDifference(), direction)
    }

    private def emptyCountValue(): Double = {
      var emptyCount = 0.0
      for (row <- 0 to 3) {
        for (column <- 0 to 3) {
          if (board.getBoard(row)(column) == 0) {
            emptyCount += 1
          }
        }
      }
      emptyCount
    }

    private def averageNeighbourDifference(): Double = {
      val realBoard = board.getBoard
      var averageDifferenceSum = 0
      for (row <- 0 to 3) {
        for (column <- 0 to 3) {
          if (realBoard(row)(column) != 0) {
            var neighbourCount = 0
            var neighbourDifferenceSum = 0
            if (row - 1 >= 0 && realBoard(row - 1)(column) != 0) {
              neighbourCount += 1
              neighbourDifferenceSum += Math.abs(realBoard(row)(column) - realBoard(row - 1)(column))
            }
            if (row + 1 <= 3 && realBoard(row + 1)(column) != 0) {
              neighbourCount += 1
              neighbourDifferenceSum += Math.abs(realBoard(row)(column) - realBoard(row + 1)(column))
            }
            if (column - 1 >= 0 && realBoard(row)(column - 1) != 0) {
              neighbourCount += 1
              neighbourDifferenceSum += Math.abs(realBoard(row)(column) - realBoard(row)(column - 1))
            }
            if (column + 1 <= 3 && realBoard(row)(column + 1) != 0) {
              neighbourCount += 1
              neighbourDifferenceSum += Math.abs(realBoard(row)(column) - realBoard(row)(column + 1))
            }
            if (neighbourCount > 0) {
              averageDifferenceSum += neighbourDifferenceSum / neighbourCount
            }
          }
        }
      }
      averageDifferenceSum
    }

  }

  override def makeMove(board: Board): Board = {
    var depth = originalDepth
    val moves = Array[Board => Board](moveUp, moveRight, moveDown, moveLeft)
    if (board.getScore < randomMovesScore) {
      depth = 2
    }
    val movesQueue = mutable.Queue[Node]()
    var rootNode = Node(board, depth, 1, 1)
    movesQueue += rootNode
    breakable {
      while (movesQueue.nonEmpty) {
        var actualNode = movesQueue.dequeue()
        if (actualNode.depth == 0) {
          break
        }
        if (actualNode.depth % 2 == 0) {
          for (direction <- 0 to 3) {
            val actualBoard = actualNode.board.copy()
            val newNode = Node(moves(direction)(new Board(actualBoard, actualNode.board.getScore, false, false)), actualNode.depth - 1, direction, 1)
            if (actualBoard.deep != newNode.board.getBoard.deep) {
              actualNode.addChild(newNode)
              movesQueue += newNode
            }
          }
        } else {
          for (row <- 0 to 3) {
            for (column <- 0 to 3) {
              for (value <- 0 to 1) {
                val actualBoard = actualNode.board.copy()
                if (actualBoard(row)(column) == 0) {
                  actualBoard(row)(column) = 2 + 2 * value
                  val newNode = Node(new Board(actualBoard, actualNode.board.getScore, false, false), actualNode.depth - 1, actualNode.direction, 0.9 - value * 0.8)
                  actualNode.addChild(newNode)
                  movesQueue += newNode
                }
              }
            }
          }
        }
      }
    }

    val value = rootNode.value()
    moves(rootNode.direction)(board)
  }

  override def makeMove(board: Board, direction: Direction): Board = {
    return null
  }

}
