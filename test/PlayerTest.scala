package players

import game.Board
import org.scalatest.FlatSpec

class PlayerTest extends FlatSpec{
  val > = Array

  val player = HumanPlayer

  "This player" should "move up correctly" in {
    val beforeBoard: Array[Array[Int]] = >(
      >(0, 0, 2, 16),
      >(0, 2, 2, 8),
      >(0, 0, 2, 4),
      >(2, 2, 2, 2)
    )
    val boardBefore = new Board(beforeBoard, 0, false, false)

    val playerBoardAfter = player.moveUp(boardBefore)

    val afterBoard: Array[Array[Int]] = >(
      >(2, 4, 4, 16),
      >(0, 0, 4, 8),
      >(0, 0, 0, 4),
      >(0, 0, 0, 2)
    )
    val boardAfter = new Board(afterBoard, 0, false, false)

    assert(playerBoardAfter.getBoard.deep == boardAfter.getBoard.deep)
  }

  "This player" should "move right correctly" in {
    val beforeBoard: Array[Array[Int]] = >(
      >(2, 0, 0, 0),
      >(2, 0, 2, 0),
      >(4, 4, 8, 8),
      >(2, 8, 2, 8)
    )
    val boardBefore = new Board(beforeBoard, 0, false, false)

    val playerBoardAfter = player.moveRight(boardBefore)

    val afterBoard: Array[Array[Int]] = >(
      >(0, 0, 0, 2),
      >(0, 0, 0, 4),
      >(0, 0, 8, 16),
      >(2, 8, 2, 8)
    )
    val boardAfter = new Board(afterBoard, 0, false, false)

    assert(playerBoardAfter.getBoard.deep == boardAfter.getBoard.deep)
  }

  "This player" should "move down correctly" in {
    val beforeBoard: Array[Array[Int]] = >(
      >(2, 0, 2, 16),
      >(0, 2, 2, 8),
      >(0, 0, 2, 4),
      >(0, 2, 2, 2)
    )
    val boardBefore = new Board(beforeBoard, 0, false, false)

    val playerBoardAfter = player.moveDown(boardBefore)

    val afterBoard: Array[Array[Int]] = >(
      >(0, 0, 0, 16),
      >(0, 0, 0, 8),
      >(0, 0, 4, 4),
      >(2, 4, 4, 2)
    )
    val boardAfter = new Board(afterBoard, 0, false, false)

    assert(playerBoardAfter.getBoard.deep == boardAfter.getBoard.deep)
  }

  "This player" should "move left correctly" in {
    val beforeBoard: Array[Array[Int]] = >(
      >(0,  0,  0,   1024),
      >(2,  0,  512, 512),
      >(64, 64, 64,  64),
      >(2,  0,  0,   2)
    )
    val boardBefore = new Board(beforeBoard, 0, false, false)

    val playerBoardAfter = player.moveLeft(boardBefore)

    val afterBoard: Array[Array[Int]] = >(
      >(1024, 0, 0, 0),
      >(2, 1024, 0, 0),
      >(128, 128, 0, 0),
      >(4, 0, 0, 0)
    )
    val boardAfter = new Board(afterBoard, 0, false, false)

    assert(playerBoardAfter.getBoard.deep == boardAfter.getBoard.deep)
  }
}