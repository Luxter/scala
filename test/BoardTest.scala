package game

import org.scalatest.FlatSpec

class BoardTest extends FlatSpec {
  val > = Array

  val randomBoard: Array[Array[Int]] = >(
    >(2, 0, 4, 8),
    >(16, 0, 64, 8),
    >(2, 2048, 256, 8),
    >(64, 8, 0, 8)
  )

  "This board" should "be initialized" in {
    val board = new Board(Array.ofDim(4, 4), 0, false, false)
    board.initialize()
    var initializedCount = 0
    board.getBoard foreach {
      case row => row foreach {
        column => if (column != 0) initializedCount += 1
      }
    }
    assert(initializedCount == 2)
  }

  "These boards" should "be equal" in {
    val firstBoard = new Board(randomBoard, 0, false, false)
    val copiedBoard = firstBoard.copy()
    assert(copiedBoard.deep == firstBoard.getBoard.deep)
  }

  "This board" can "perform move" in {
    val moveBoard: Array[Array[Int]] = >(
      >(2, 16, 4, 8),
      >(16, 8, 64, 1024),
      >(2, 2048, 256, 8),
      >(64, 8, 512, 0)
    )
    val board = new Board(moveBoard, 0, false, false)

    assert(board.canPerformMove)
  }

  "This board" can "perform merge vertically" in {
    val mergeBoard: Array[Array[Int]] = >(
      >(2, 16, 4, 8),
      >(16, 8, 64, 1024),
      >(2, 2048, 256, 8),
      >(64, 8, 512, 8)
    )
    val board = new Board(mergeBoard, 0, false, false)

    assert(board.canPerformMove)
  }

  "This board" can "perform merge horizontally" in {
    val mergeBoard: Array[Array[Int]] = >(
      >(2, 16, 4, 8),
      >(16, 8, 64, 1024),
      >(2, 2048, 256, 16),
      >(64, 8, 8, 8)
    )
    val board = new Board(mergeBoard, 0, false, false)

    assert(board.canPerformMove)
  }

  "This board" can "not perform move" in {
    val mergeBoard: Array[Array[Int]] = >(
      >(2, 16, 4, 8),
      >(16, 8, 64, 1024),
      >(2, 2048, 256, 8),
      >(64, 8, 512, 32)
    )
    val board = new Board(mergeBoard, 0, false, false)

    assert(!board.canPerformMove)
  }

  "This board" should "contain 2048" in {
    val mergeBoard: Array[Array[Int]] = >(
      >(2, 16, 4, 8),
      >(16, 8, 64, 1024),
      >(2, 2048, 256, 8),
      >(64, 8, 512, 32)
    )
    val board = new Board(mergeBoard, 0, false, false)

    assert(board.contains2048)
  }

  "This board" should "not contain 2048" in {
    val mergeBoard: Array[Array[Int]] = >(
      >(2, 16, 4, 8),
      >(16, 8, 64, 1024),
      >(2, 16, 256, 8),
      >(64, 8, 512, 32)
    )
    val board = new Board(mergeBoard, 0, false, false)

    assert(!board.contains2048)
  }

  "This board" should " have empty elements" in {
    val mergeBoard: Array[Array[Int]] = >(
      >(0, 16, 4, 0),
      >(16, 8, 64, 1024),
      >(2, 16, 0, 8),
      >(64, 8, 512, 32)
    )
    val board = new Board(mergeBoard, 0, false, false)

    assert(board.countEmptyElements == 3)
  }

  "This board" should "not have empty elements" in {
    val mergeBoard: Array[Array[Int]] = >(
      >(2, 16, 4, 8),
      >(16, 8, 64, 1024),
      >(2, 16, 256, 8),
      >(64, 8, 512, 32)
    )
    val board = new Board(mergeBoard, 0, false, false)

    assert(board.countEmptyElements == 0)
  }
}