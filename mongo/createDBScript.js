var db = connect('127.0.0.1:27017/2048GameDB');
print('* Connected to database');

db.dropDatabase();

db.Save.drop();
db.createCollection("GameSave", {
    capped: true,
    autoIndexID: true,
    size: 512000000,
    max: 260,
    validator: {
        $and: [{
            name: {
                $type: "string",
                $unique: true
            },
            game: {
                $type: "string",
                $exists: true
            },
            score: {
                $type: "string",
                $exists: true
            }
        }]
    },
    validationAction: "error"
});