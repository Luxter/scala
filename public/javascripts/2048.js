function newHumanGame() {
    jQuery.ajax({
        url: "/human",
        success: function (data, status) {
            board = data.board
            score = data.score
        },
        async: false
    })

    showBoard()
    enableKeyboardListener()
}

function newComputerGame() {
    jQuery.ajax({
        url: "/computer",
        data: {level: $('#level').val()},
        success: function (data, status) {
            board = data.board
            score = data.score
        },
        async: false
    })
    showBoard()
    enableComputerKeyboardListener()
}

function loadGame() {
    selected = JSON.parse(JSON.stringify($('#table').bootstrapTable('getSelections')))[0]

    if (selected) {
        var value = selected.name;

        jQuery.ajax({
            url: "/load",
            data: {name: value},
            success: function (data, status) {
                board = data.board
                score = data.score
                if (data.is2048){
                    $('#winModal').modal('show');
                } else {
                    if (!data.canMove){
                        $('#loseModal').modal('show');
                    }
                }
            },
            async: false
        })
    } else {
        alert('No game save selected.')
    }
    showBoard()
    enableKeyboardListener()
}

function saveGame() {
    jQuery.ajax({
        url: "/save",
        data: {name: $("#name").val()},
        success: function (data, status) {
            board = data.board
            score = data.score
            if (data.is2048){
                $('#winModal').modal('show');
            } else {
                if (!data.canMove){
                    $('#loseModal').modal('show');
                }
            }
        },
        async: false
    });
    showBoard()
    enableKeyboardListener()
}

$(function () {
    $('#table').bootstrapTable({
        url: '/saves'

    });
});

//$("#saveGame").submit(function(event){
//jQuery.ajax({
//        url: "/save",
//        success: function(data, status) {
//            board = data.board
//        },
//        async: false
//    })
//    showBoard()
//    enableKeyboardListener()
//});

function showBoard() {
    $("#board").empty()
    for (var i = 0; i < board.length; i++) {
        $("#board").append('<div class="row" id="row' + i + '"/>')
        for (var j = 0; j < board[i].length; j++) {
            var value = board[i][j] == 0 ? "" : board[i][j]
            $("#row" + i).append('<div class="col-lg-3 cell cell' + board[i][j] + '">' + value + '</div>')
        }
    }
    $("#score").text("Score: " + score)
}

function enableKeyboardListener() {
    document.onkeydown = function (e) {
        document.onkeydown = null;
        switch (e.keyCode) {
            case 37:
                jQuery.ajax({
                    url: "/left",
                    success: function (data, status) {
                        board = data.board
                        score = data.score
                        if (data.is2048){
                            $('#winModal').modal('show');
                        } else {
                            if (!data.canMove){
                                $('#loseModal').modal('show');
                            }
                        }
                    },
                    async: false
                })
                break
            case 38:
                jQuery.ajax({
                    url: "/up",
                    success: function (data, status) {
                        board = data.board
                        score = data.score
                        if (data.is2048){
                            $('#winModal').modal('show');
                        } else {
                            if (!data.canMove){
                                $('#loseModal').modal('show');
                            }
                        }
                    },
                    async: false
                })
                break
            case 39:
                jQuery.ajax({
                    url: "/right",
                    success: function (data, status) {
                        board = data.board
                        score = data.score
                        if (data.is2048){
                            $('#winModal').modal('show');
                        } else {
                            if (!data.canMove){
                                $('#loseModal').modal('show');
                            }
                        }
                    },
                    async: false
                })
                break
            case 40:
                jQuery.ajax({
                    url: "/down",
                    success: function (data, status) {
                        board = data.board
                        score = data.score
                        if (data.is2048){
                            $('#winModal').modal('show');
                        } else {
                            if (!data.canMove){
                                $('#loseModal').modal('show');
                            }
                        }
                    },
                    async: false
                })
                break
        }
        enableKeyboardListener()
        showBoard()
    };
}

function enableComputerKeyboardListener() {
    document.onkeydown = function (e) {
        document.onkeydown = null;
        switch (e.keyCode) {
            case 32:
                jQuery.ajax({
                    url: "/solve",
                    success: function (data, status) {
                        board = data.board
                        score = data.score
                        if (data.is2048){
                            $('#winModal').modal('show');
                        } else {
                            if (!data.canMove){
                                $('#loseModal').modal('show');
                            }
                        }
                    },
                    async: false
                })
                break
        }
        enableComputerKeyboardListener()
        showBoard()
    };
}
